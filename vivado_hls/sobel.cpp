#include "sobel.hpp"
#include <hls_math.h>
#include <ap_int.h>
#include <ap_fixed.h>

void split_stream(hls::Mat<X, Y, HLS_8UC1>& src,
		hls::Mat<X, Y, HLS_16SC1>& out1, hls::Mat<X, Y, HLS_16SC1>& out2) {

	Scalar<1, uint8_t> pix_in;
	Scalar<1, int16_t> pix_out;

	uint32_t X = src.rows;
	uint32_t Y = src.cols;

	for (int i = 0; i < X * Y; ++i) {
		src >> pix_in;
		pix_out = (int16_t)(pix_in.val[0]);
		out1 << pix_out;
		out2 << pix_out;
	}
}

void combine_gradients(
		hls::Mat<X, Y, HLS_16SC1>& src_x,
		hls::Mat<X, Y, HLS_16SC1>& src_y,
		hls::Mat<X, Y, HLS_8UC1>& edges)
		{

	unsigned int image_x = src_x.rows;
	unsigned int image_y = src_x.cols;

	std::cout << image_x << " " << image_y << std::endl;

	hls::Scalar<1, int16_t> pix_x, pix_y;
	hls::Scalar<1, uint8_t> out;

	for (int i = 0; i < image_x * image_y; ++i) {
	#pragma HLS PIPELINE
		src_x >> pix_x;
		src_y >> pix_y;

		int16_t val_x = pix_x.val[0];
		int16_t val_y = pix_y.val[0];

		out = (uint8_t)hls::sqrt(val_x * val_x + val_y * val_y);

		edges << out;
	}

}

void thr (
		hls::Mat<X, Y, HLS_8UC1>& sobel_out,
		hls::Mat<X, Y, HLS_8UC1>& thr_out,
		unsigned char low_thr,
		unsigned char high_thr
		) {


	hls::Scalar<1, uint8_t> pix_in;
	hls::Scalar<1, uint8_t> pix_out;

	uint32_t X = sobel_out.rows;
	uint32_t Y = sobel_out.cols;

	for (int i = 0; i < X*Y; ++i) {
#pragma HLS PIPELINE
		sobel_out >> pix_in;

		if (pix_in.val[0] >= high_thr) {
			pix_out = 255;
		} else if (pix_in.val[0] <= low_thr) {
			pix_out = 0;
		} else {
			pix_out = 255;
		}

		thr_out << pix_out;

	}
}

void sobel(
		hls::stream<ap_axiu<32,1,1,1> >& in_frame,
		hls::stream<ap_axiu<32,1,1,1> >& out_frame,
		unsigned int image_x,
		unsigned int image_y
	)
{
#pragma HLS RESOURCE variable=in_frame core=AXIS metadata="-bus_bundle INPUT_STREAM"

#pragma HLS RESOURCE variable=out_frame core=AXIS metadata="-bus_bundle OUTPUT_STREAM"

#pragma HLS RESOURCE variable=image_x core=AXI_SLAVE metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE variable=image_y core=AXI_SLAVE metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE variable=return core=AXI_SLAVE metadata="-bus_bundle CONTROL_BUS"


	hls::Mat<X, Y, HLS_8UC1> src(image_x, image_y);
	hls::Mat<X, Y, HLS_16SC1> sobel_x_in(image_x, image_y);
	hls::Mat<X, Y, HLS_16SC1> sobel_y_in(image_x, image_y);
	hls::Mat<X, Y, HLS_16SC1> sobel_x(image_x, image_y);
	hls::Mat<X, Y, HLS_16SC1> sobel_y(image_x, image_y);
	hls::Mat<X, Y, HLS_8UC1> threshold(image_x, image_y);
	hls::Mat<X, Y, HLS_8UC1> out(image_x, image_y);

#pragma HLS DATAFLOW
	hls::AXIvideo2Mat<32, X, Y, HLS_8UC1>(in_frame, src);
	split_stream(src, sobel_x_in, sobel_y_in);
	hls::Sobel<0, 1, 3>(sobel_x_in, sobel_x);
	hls::Sobel<1, 0, 3>(sobel_y_in, sobel_y);
	combine_gradients(sobel_x, sobel_y, threshold);
	thr(threshold, out, 90, 180);
	hls::Mat2AXIvideo<32, X, Y, HLS_8UC1>(out, out_frame);

}
