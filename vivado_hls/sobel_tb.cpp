#include "sobel.hpp"

#include <hls_opencv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <string>

using std::string;
using std::cerr;

int main() {

	// Images can be found in solution1/csim/build
	// Alternatively, use a full path
	const string src_img = "gray.jpg";

	IplImage* src=cvLoadImage(src_img.c_str());

	if (!src) {
		cerr << "Can't load image!\n";
		exit(1);
	}

	IplImage* dst=cvCreateImage(cvGetSize(src),IPL_DEPTH_8U, 1);

	if (!src || !src->imageData) {
		cerr << "Can't convert image\n";
		exit(1);
	}

	hls::stream<ap_axiu<32,1,1,1> > src_axi;
	hls::stream<ap_axiu<32,1,1,1> > dst_axi;

	IplImage2AXIvideo(src, src_axi);

	sobel(src_axi, dst_axi, src->height, src->width);

	AXIvideo2IplImage(dst_axi, dst);

	cv::Mat cv_dst = cv::cvarrToMat(dst);

	cv::imshow("outImage", cv_dst);
	cv::waitKey(0);

	cv::imwrite("edges.png", cv_dst);

	cvReleaseImage(&src);
	cvReleaseImage(&dst);

	return 0;
}
