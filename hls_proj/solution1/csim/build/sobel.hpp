#pragma once

#include <iostream>
#include <string>
#include <bitset>
#include <climits>
#include <hls_video.h>

using namespace hls;

static const unsigned int Y = 1920;
static const unsigned int X = 1080;
static const unsigned int SIZE = X * Y;

static const unsigned int MAX_Y = 1920;
static const unsigned int MAX_X = 1080;

static const unsigned int K_Y = 3;
static const unsigned int K_X = 3;

void sobel(
		hls::stream<ap_axiu<32, 1, 1, 1> >& in_frame,
		hls::stream<ap_axiu<32, 1, 1, 1> >& out_frame,
		unsigned int image_x,
		unsigned int image_y
		);

