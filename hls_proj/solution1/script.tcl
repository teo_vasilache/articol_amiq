############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project sobel
add_files articol_amiq/vivado_hls/sobel.cpp
add_files articol_amiq/vivado_hls/sobel.hpp
add_files articol_amiq/vivado_hls/sobel.cpp
add_files articol_amiq/vivado_hls/sobel.hpp
add_files -tb articol_amiq/vivado_hls/sobel_tb.cpp
add_files -tb articol_amiq/vivado_hls/sobel_tb.cpp
open_solution "solution1"
set_part {xc7z020clg484-1} -tool vivado
create_clock -period 10 -name default
#source "./sobel/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
